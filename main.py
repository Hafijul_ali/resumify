from flask import Flask, request, render_template

app = Flask(__name__)


@app.route('/', methods=["GET", "POST"])
def index():
    return  render_template("index.html")


@app.route("/resume", methods=["GET", "POST"])
def display_resume():
    if request.method == "POST" and request.form is not None:
        return  render_template('resume.html', result=request.form)
    return  "<h1><center>Please, input some value</center></h1>"
